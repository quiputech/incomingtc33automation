/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.incomingprocesstc33.utils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
/**
 *
 * @author Estephany
 */
public class S3Interaction {
    
        
    public static String getReportFromS3(String fileName, String bucket, LambdaLogger logger, String fechaIncoming,
                                       String nameOutFile, String filePath, HashMap<String, String> newMerchant) {
        String errorMsg = "";
        try {
            
            File source_dir = new File("/tmp");
            
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
 
            logger.log("Incoming file " + fileName + " download started from bucket " + bucket);
            GetObjectRequest getObjectRequest = new GetObjectRequest("/" + bucket, fileName);
            s3Client.getObject(getObjectRequest, new File("/tmp/" + fileName)); 
            logger.log("Incoming file " + fileName + " download finished");
            
            ProcessIncomingFile.listFiles(source_dir, logger);
     
            File file = new File("/tmp/" + fileName);
            FileInputStream DATFile = new FileInputStream(file);
            InputStream fileContent = DATFile;
            if(fileContent != null){
                logger.log("Recovered file from /tmp/" + fileName );
               
                Map<String, Object[]> data = new TreeMap<String, Object[]>();
                data.put("1", new Object[]{"CODIGO COMERCIO", "RECORD TYPE", "ISSUER-ACQUIRER INDICATOR", "REMOTE TERMINAL INDICATOR",
                    "PRODUCT ID", "BUSINESS APPLICATION IDENTIFIER", "ACCOUNT FUNDING SOURCE", "AFFILIATE BIN", "SETTLEMENT DATE",
                    "FECHA", "TRANSACTION IDENTIFIER", "VALIDATION CODE", "RETRIEVAL REFERENCE NUMBER", "TRACE NUMBER", "BATCH NUMBER", "REQUEST MESSAGE TYPE",
                    "RESPONSE CODE", "PROCESSING CODE", "CARD NUMBER", "TRANSACTION AMOUNT", "SIGNED AMOUNT", "COMISION (1.89%)", "FEE INTERCAMBIO",
                    "CURRENCY CODE", "OLD MERCHANT"});
                int i;
                int x20 = 2;
                try {
                    //TRAMIENTO DE ARCHIVO
                    BufferedReader br = new BufferedReader(new InputStreamReader(fileContent, "UTF-8"));
                    String line;
                    int z = 1;
                    while ((line = br.readLine()) != null) {
                        String tc33_V22200 = null;
                        String indexMapV22220 = null;
                        String indexMap = null;
                        String indexMapRVI = null;
                        if (line.startsWith("33")) {
                            if (line.contains("V22200")) {
                                tc33_V22200 = line;
                                z = x20;
                                Object[] inputConvert = ProcessIncomingFile.process(tc33_V22200, fechaIncoming, String.valueOf(x20), logger);
                                if (inputConvert != null) {
                                    data.put(String.valueOf(x20), inputConvert);
                                    x20++;
                                }
                            }
                            if (line.contains("V22220")) {
                                Object[] completeData = data.get(String.valueOf(z));
                                indexMapV22220 = line;
                                Object[] inputConvert = ProcessIncomingFile.completeIndexMapV22220(indexMapV22220, completeData, logger,newMerchant);
                                if (inputConvert != null) {
                                    data.put(String.valueOf(z), inputConvert);
                                }
                            }
                            if (line.contains("V22225")) {
                                Object[] completeData = data.get(String.valueOf(z));
                                indexMap = line;
                                Object[] inputConvert = ProcessIncomingFile.completeIndex(indexMap, completeData, logger);
                                if (inputConvert != null) {
                                    data.put(String.valueOf(z), inputConvert);
                                }
                            }
                            if (line.contains("V22261")) {
                                Object[] completeData = data.get(String.valueOf(z));
                                indexMapRVI = line;
                                Object[] inputConvert = ProcessIncomingFile.completeIndexRVI(indexMapRVI, completeData, logger);
                                if (inputConvert != null) {
                                    data.put(String.valueOf(z), inputConvert);
                                }
                            }
                        }

                    }
                    
                    logger.log("Incoming file processed");

                    br.close();

                } catch (IOException e) {
                    logger.log(e.getLocalizedMessage());
                }          
                logger.log("Size " + data.size() + " key-value mappings");
                if (ProcessIncomingFile.createFileTemp(data, nameOutFile, logger, filePath)){
                    logger.log("Incoming file processed successfully and report TC33 saved in " + filePath);
                }else{
                    logger.log("Error creating TC33 REPORT");
                }
            }                   
            
        } catch (AmazonServiceException e) {
            String errorCode = e.getErrorCode();
            if (errorCode.equals("NoSuchKey")) {
                logger.log("Incoming file "  + fileName + " not found in bucket " + bucket);
                errorMsg = "Archivo no encontrado";
                return errorMsg;
            } else{
                logger.log(e.getLocalizedMessage());
                errorMsg = e.getLocalizedMessage();
            }
        } catch (FileNotFoundException ex) {
            logger.log(ex.getLocalizedMessage());
            errorMsg = ex.getLocalizedMessage();
        }
        return errorMsg;
    }   
    
    public static String uploadReportToS3(String fileName, String bucket, LambdaLogger logger, String filePath) {
        String errorMsg = "";
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
            // Upload a file as a new object with ContentType and title specified.
            String targetKey = fileName + ".csv";
            String fileOutName = filePath + fileName + ".csv";
            PutObjectRequest request = new PutObjectRequest(bucket, targetKey, new File(fileOutName));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            request.setMetadata(metadata);
            s3Client.putObject(request);
            return errorMsg;
        } catch (AmazonServiceException e) {
            logger.log(e.getLocalizedMessage());
            errorMsg = "error al copiar el archivo en el bucket AWS " + bucket;
            return errorMsg;
        }
    }
    
}
