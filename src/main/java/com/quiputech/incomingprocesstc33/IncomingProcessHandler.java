/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.incomingprocesstc33;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.ScheduledEvent;
import com.quiputech.incomingprocesstc33.utils.DateFormatUtil;
import com.quiputech.incomingprocesstc33.utils.S3Interaction;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import com.quiputech.incomingprocesstc33.utils.Email;
import java.util.TimeZone;
/**
 *
 * @author Estephany
 */
public class IncomingProcessHandler implements RequestHandler<ScheduledEvent, Void>{
   
    private static final HashMap<String, String> newMerchant = new HashMap<String, String>();
    
    @Override
    public Void handleRequest(ScheduledEvent input, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("In Handler: Executing " + context.getFunctionName());
        
        String manualExecution = System.getenv("MANUAL_EXECUTION");
        String in_bucket = System.getenv("INCOMING_FILES_BUCKET");
        String out_bucket = System.getenv("TC33_REPORTS_BUCKET");   
        String incomingFileName = getFileName(manualExecution, logger);
        String incomingFileNameReplaced = incomingFileName.replace(".DAT", "");
               
        try {
            Date date = DateFormatUtil.formatStringtoDate("yyyyMMdd", incomingFileNameReplaced.substring(incomingFileNameReplaced.length() - 8, incomingFileNameReplaced.length()));
            String dateIncoming = DateFormatUtil.formatDateToString("ddMMyyyy", date);
            String nameOutFile = "TC33_" + incomingFileNameReplaced + "_" + dateIncoming;
            String filePath = "/tmp/";
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.HOUR, -5);
            String currentDate = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(c.getTime());
            //Extraer el archivo del bucket de origen
            String errorMsgGetFromS3 = S3Interaction.getReportFromS3(incomingFileName, in_bucket,logger, dateIncoming, nameOutFile, filePath, newMerchant);
            if(!errorMsgGetFromS3.isEmpty()){
                String message = "Fecha y hora de proceso: " + currentDate + ". Error: " + errorMsgGetFromS3;
                sendErrorEmailNotification(message, logger);
            }
            //Subir reporte TC33 a bucket de destino  
            String errorMsgPutS3 = S3Interaction.uploadReportToS3(nameOutFile, out_bucket, logger, filePath);            
            if(errorMsgPutS3.isEmpty()){
                logger.log("TC33 report uploaded to bucket " + out_bucket);
                //Delete incoming file and tc33 report from /tmp/ after uploading tc33 report to target bucket
                deleteFiles(filePath, incomingFileName, nameOutFile, logger);    
                //Send email notification                
                String message = "Proceso de descarga de archivo TC33. Fecha y hora de proceso: " + currentDate;
                sendSuccessfulEmailNotification(message, logger);
            }else{
                logger.log("Error uploading TC33 report to bucket " + out_bucket);
                //Send email notification 
                String message = "Fecha y hora de proceso: " + currentDate + ". Error: " + errorMsgPutS3;
                sendErrorEmailNotification(message, logger);
            }
        } catch (ParseException ex) {
            logger.log(ex.getLocalizedMessage());
        }

        return null;
    }
    
    public static void deleteFiles(String filePath, String fileName, String nameOutFile, LambdaLogger logger){
        File incomingFile = new File(filePath + fileName);
        if(incomingFile.delete()){
            logger.log("File " + fileName + " deleted from " + filePath);
        }else{
            logger.log("File " + fileName + " couldn't be deleted from " + filePath);
        }
        File tc33Report = new File(filePath + nameOutFile + ".csv");
        if(tc33Report.delete()){
            logger.log("File " + nameOutFile + ".csv deleted from " + filePath);
        }else{
            logger.log("File " + nameOutFile + ".csv couldn't be deleted from " + filePath);
        }
    }
    
    public static String getFileName(String manualExecution, LambdaLogger logger){
        String fileName = "";
        if(manualExecution.equalsIgnoreCase("NO") || manualExecution.isEmpty()){
            //Se forma el nombre del archivo con la fecha del día actual
            String currentDate = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
            fileName = "INCOMINGVIS" + currentDate + ".DAT";  
            logger.log("Automatic execution");
        }else{
            //Si es que el flag para ejecución manual está activo, se recupera el nombre del archivo a procesar
            fileName = System.getenv("FILENAME");    
            logger.log("Manual execution");
        }
        return fileName;
    }
    
    public static void sendSuccessfulEmailNotification(String message, LambdaLogger logger){
        String sender = System.getenv("EMAIL_SENDER");  
        String recipients = System.getenv("EMAIL_RECIPIENTS");
        String[] recipients_list = recipients.split(",");
        String subject = System.getenv("SUBJECT_SUCCESSFUL");  
        
        Email.sendEmailNotification(sender, subject, message, recipients_list, null, logger);
    }
    
    public static void sendErrorEmailNotification(String message, LambdaLogger logger){
        String sender = System.getenv("EMAIL_SENDER");  
        String recipients = System.getenv("EMAIL_RECIPIENTS");
        String[] recipients_list = recipients.split(",");
        String subject = System.getenv("SUBJECT_ERROR");  
        
        Email.sendEmailNotification(sender, subject, message, recipients_list, null, logger);
    }
}
