/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.incomingprocesstc33.utils;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/**
 *
 * @author Estephany
 */
public class Email {
    public static void sendEmailNotification(String sender, String subject, String message, String[] recipients, String[] attachedFiles, LambdaLogger logger) {
        MimeMessage mMessage = null;
        try {
            String DefaultCharSet = MimeUtility.getDefaultJavaCharset();
            Session session = Session.getDefaultInstance(new Properties());
            mMessage = new MimeMessage(session);
            mMessage.setSubject(subject, "UTF-8");
            mMessage.setFrom(new InternetAddress(sender));
            InternetAddress[] aRecipients = new InternetAddress[recipients.length];
            for (int i = 0; i < aRecipients.length; i++) {
                if (recipients[i].trim().length() > 0) {
                    aRecipients[i] = new InternetAddress(recipients[i].trim());
                }
            }
            mMessage.setRecipients(RecipientType.TO, aRecipients);
            MimeMultipart msg_body = new MimeMultipart("alternative");
            MimeBodyPart wrap = new MimeBodyPart();
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setContent(MimeUtility
                    .encodeText(message, DefaultCharSet, "B"), "text/plain; charset=UTF-8");
            textPart.setHeader("Content-Transfer-Encoding", "base64");

            msg_body.addBodyPart(textPart);
            wrap.setContent(msg_body);

            MimeMultipart msg = new MimeMultipart("mixed");
            mMessage.setContent(msg);

            msg.addBodyPart(wrap);
            MimeBodyPart att = null;
            javax.activation.DataSource fds = null;
            if (attachedFiles != null) {
                for (String pathFile : attachedFiles) {
                    att = new MimeBodyPart();
                    fds = new FileDataSource(pathFile);
                    att.setDataHandler(new DataHandler(fds));
                    att.setFileName(fds.getName());

                    msg.addBodyPart(att);
                }
            }
            
            AmazonSimpleEmailService client
                    = AmazonSimpleEmailServiceClientBuilder.standard()
                            .withRegion(Regions.US_EAST_1).build(); 

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            mMessage.writeTo(outputStream);
            RawMessage rawMessage
                    = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

            SendRawEmailRequest rawEmailRequest
                    = new SendRawEmailRequest(rawMessage);

            client.sendRawEmail(rawEmailRequest);
            logger.log("Email sent.");
        } catch (IOException | MessagingException e) {
            logger.log(e.getLocalizedMessage());
        }
    }
    
    
}
