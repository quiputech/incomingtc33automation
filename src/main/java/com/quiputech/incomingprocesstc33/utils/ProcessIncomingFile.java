/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.incomingprocesstc33.utils;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Estephany
 */
public class ProcessIncomingFile {
    public static Object[] process(String tc33_V22200, String fechaIncoming, String count, LambdaLogger logger) {
        Object[] data = null;
        
        try {
            
            if (tc33_V22200 == null) {
                return null;
            }
            String inp = "", inp1 = "", inp2 = "", inp4 = "", inp6 = "", inp7 = "", inp10 = "";
            String inp11 = "", inp12 = "", inp13 = "", inp14 = "", inp15 = "", inp16 = "", inp17 = "", inp18 = "", inp19 = "", inp20 = "";
            String inp21 = "", inp22 = "", inp23 = "", inp24 = "", inp25 = "0", inp26 = "", inp27 = "", inp29 = "";
            int i = 0;
            tc33_V22200 = tc33_V22200.substring(34);
            inp1 = tc33_V22200.substring(i, i + 6);
            i = 6;
            inp2 = tc33_V22200.substring(i, i + 1);
            i = 7;
            i = 17;
            inp4 = tc33_V22200.substring(i, i + 1);
            i = 18;
            i = 19;
            inp6 = tc33_V22200.substring(i, i + 2);
            i = 21;
            inp7 = tc33_V22200.substring(i, i + 2);
            i = 23;
            i = 24;
            i = 26;
            inp10 = tc33_V22200.substring(i, i + 1);
            i = 27;
            inp11 = tc33_V22200.substring(i, i + 10);
            i = 37;
            inp12 = tc33_V22200.substring(i, i + 6);
            
            inp13 = fechaIncoming;
            
            i = 43;
            inp14 = tc33_V22200.substring(i, i + 15); //TID
            i = 58;
            inp15 = tc33_V22200.substring(i, i + 4);
            i = 62;
            inp16 = tc33_V22200.substring(i, i + 12);
            i = 74;
            inp17 = tc33_V22200.substring(i, i + 6);
            i = 80;
            inp18 = tc33_V22200.substring(i, i + 4);
            i = 84;
            inp19 = tc33_V22200.substring(i, i + 4);
            i = 88;
            inp20 = tc33_V22200.substring(i, i + 2);
            i = 90;
            inp21 = tc33_V22200.substring(i, i + 6);
            i = 96;
            inp22 = maskCard(tc33_V22200.substring(i, i + 19));
            i = 115;
            inp23 = tc33_V22200.substring(i, i + 12);
            
            inp24 = String.valueOf(signedAmount(inp23));
            
            if (inp7.equalsIgnoreCase("PS")) {//QR
                if (inp19.equalsIgnoreCase("0200")) {
                    if (inp21.equalsIgnoreCase("260000")) //OCT
                    {
                        inp25 = "0";
                        inp26 = "0";
                    } else if (inp21.equalsIgnoreCase("100000")) { //AFT
                        inp25 = String.valueOf(Double.parseDouble(inp24) * 1.89 / 100);
                        inp26 = String.valueOf(Double.parseDouble(inp25) * 50 / 100);
                    }
                } else if (inp19.equalsIgnoreCase("0420")) { //REVERSA

                    inp25 = String.valueOf(Double.parseDouble(inp24) * 1.89 / 100 * (-1));
                    inp26 = String.valueOf(Double.parseDouble(inp25) * 50 / 100);
                }
            } else if (inp7.equalsIgnoreCase("PP")) {//PP
                if (inp19.equalsIgnoreCase("0200")) {
                    if (inp21.equalsIgnoreCase("260000")) //OCT
                    {
                        inp25 = "0";
                        inp26 = String.valueOf(Double.parseDouble("0.05"));
                    } else if (inp21.equalsIgnoreCase("100000")) { //AFT
                        inp25 = "0";
                        inp26 = "0.13";
                    }
                } else if (inp19.equalsIgnoreCase("0420")) { //REVERSA                        

                    inp25 = "0";
                    inp26 = String.valueOf(Double.parseDouble("0.13") * (-1));
                } else if (inp19.equalsIgnoreCase("0422")) {
                    inp25 = "0";
                    inp26 = String.valueOf(Double.parseDouble("0.05") * (-1));
                }
                
            } else {
                inp25 = "0";
                inp26 = "0";
            }
            
            i = 127;
            inp27 = tc33_V22200.substring(i, i + 3);
            data = new Object[]{inp,
                inp1, inp2, inp4, inp6, inp7, inp10,
                inp11, inp12, inp13, inp14, inp15, inp16, inp17, inp18, inp19, inp20,
                inp21, inp22, inp23, inp24, inp25, inp26, inp27, inp29
            };
            return data;
        } catch (NumberFormatException e) {
            logger.log(e.getLocalizedMessage());
            return null;
        }
    }
    
    public static Object[] completeIndexMapV22220(String indexMapV22220, Object[] completeData, LambdaLogger logger, HashMap<String, String> newMerchant) {
        Object[] data = null;
        String inp = "", inp1 = "", inp2 = "", inp4 = "", inp6 = "", inp7 = "", inp10 = "";
        String inp11 = "", inp12 = "", inp13 = "", inp14 = "", inp15 = "", inp16 = "", inp17 = "", inp18 = "", inp19 = "", inp20 = "";
        String inp21 = "", inp22 = "", inp23 = "", inp24 = "", inp25 = "0", inp26 = "", inp27 = "", inp29 = "";
        try {
            int i = 0;
            for (Object input : completeData) {
                if (i == 0) {
                    inp = input.toString();
                }
                if (i == 1) {
                    inp1 = input.toString();
                }
                if (i == 2) {
                    inp2 = input.toString();
                }
                if (i == 3) {
                    inp4 = input.toString();
                }
                
                if (i == 4) {
                    inp6 = input.toString();
                }
                if (i == 5) {
                    inp7 = input.toString();
                }
                if (i == 6) {
                    inp10 = input.toString();
                }
                if (i == 7) {
                    inp11 = input.toString();
                }
                if (i == 8) {
                    inp12 = input.toString();
                }
                if (i == 9) {
                    inp13 = input.toString();
                }
                if (i == 10) {
                    inp14 = input.toString();
                }
                if (i == 11) {
                    inp15 = input.toString();
                }
                if (i == 12) {
                    inp16 = input.toString();
                }
                if (i == 13) {
                    inp17 = input.toString();
                }
                if (i == 14) {
                    inp18 = input.toString();
                }
                if (i == 15) {
                    inp19 = input.toString();
                }
                if (i == 16) {
                    inp20 = input.toString();
                }
                if (i == 17) {
                    inp21 = input.toString();
                }
                if (i == 18) {
                    inp22 = input.toString();
                }
                if (i == 19) {
                    inp23 = input.toString();
                }
                if (i == 20) {
                    inp24 = input.toString();
                }
                if (i == 21) {
                    inp25 = input.toString();
                }
                if (i == 22) {
                    inp26 = input.toString();
                }
                if (i == 23) {
                    inp27 = input.toString();
                }
                
                if (i == 24) {
                    inp29 = input.toString();
                }
                
                i++;
            }
            String cadenaV22220 = "";
            if (indexMapV22220 != null) {
                cadenaV22220 = indexMapV22220;
                inp = cadenaV22220.substring(60, 75);
            }
            try {
                String key = inp.trim() + "_" + inp22.substring(0, 6);
                if (newMerchant!= null && newMerchant.get(key) != null) {
                    inp29 = inp;
                    inp = newMerchant.get(key);
                }
            } catch (Exception e) {
                logger.log(e.getLocalizedMessage());
            }
            
            data = new Object[]{inp,
                inp1, inp2, inp4, inp6, inp7, inp10,
                inp11, inp12, inp13, inp14, inp15, inp16, inp17, inp18, inp19, inp20,
                inp21, inp22, inp23, inp24, inp25, inp26, inp27, inp29};
            return data;
        } catch (Exception e) {
            return null;
        }
    }
    
    public static Object[] completeIndex(String indexMap, Object[] completeData, LambdaLogger logger) {
        Object[] data = null;
        String inp = "", inp1 = "", inp2 = "", inp4 = "", inp6 = "", inp7 = "", inp10 = "";
        String inp11 = "", inp12 = "", inp13 = "", inp14 = "", inp15 = "", inp16 = "", inp17 = "", inp18 = "", inp19 = "", inp20 = "";
        String inp21 = "", inp22 = "", inp23 = "", inp24 = "", inp25 = "0", inp26 = "", inp27 = "", inp29 = "";
        try {
            int i = 0;
            for (Object input : completeData) {
                if (i == 0) {
                    inp = input.toString();
                }
                if (i == 1) {
                    inp1 = input.toString();
                }
                if (i == 2) {
                    inp2 = input.toString();
                }
                if (i == 3) {
                    inp4 = input.toString();
                }
                
                if (i == 4) {
                    inp6 = input.toString();
                }
                if (i == 5) {
                    inp7 = input.toString();
                }
                if (i == 6) {
                    inp10 = input.toString();
                }
                if (i == 7) {
                    inp11 = input.toString();
                }
                if (i == 8) {
                    inp12 = input.toString();
                }
                if (i == 9) {
                    inp13 = input.toString();
                }
                if (i == 10) {
                    inp14 = input.toString();
                }
                if (i == 11) {
                    inp15 = input.toString();
                }
                if (i == 12) {
                    inp16 = input.toString();
                }
                if (i == 13) {
                    inp17 = input.toString();
                }
                if (i == 14) {
                    inp18 = input.toString();
                }
                if (i == 15) {
                    inp19 = input.toString();
                }
                if (i == 16) {
                    inp20 = input.toString();
                }
                if (i == 17) {
                    inp21 = input.toString();
                }
                if (i == 18) {
                    inp22 = input.toString();
                }
                if (i == 19) {
                    inp23 = input.toString();
                }
                if (i == 20) {
                    inp24 = input.toString();
                }
                if (i == 21) {
                    inp25 = input.toString();
                }
                if (i == 22) {
                    inp26 = input.toString();
                }
                if (i == 23) {
                    inp27 = input.toString();
                }
                if (i == 24) {
                    inp29 = input.toString();
                }
                i++;
            }
            
            data = new Object[]{inp,
                inp1, inp2, inp4, inp6, inp7, inp10,
                inp11, inp12, inp13, inp14, inp15, inp16, inp17, inp18, inp19, inp20,
                inp21, inp22, inp23, inp24, inp25, inp26, inp27, inp29};
            return data;
        } catch (Exception e) {
            logger.log(e.getLocalizedMessage());
            return null;
        }
    }
    
    public static Object[] completeIndexRVI(String indexMapRVI, Object[] completeData, LambdaLogger logger) {
        Object[] data = null;
        String inp = "", inp1 = "", inp2 = "", inp4 = "", inp6 = "", inp7 = "", inp10 = "";
        String inp11 = "", inp12 = "", inp13 = "", inp14 = "", inp15 = "", inp16 = "", inp17 = "", inp18 = "", inp19 = "", inp20 = "";
        String inp21 = "", inp22 = "", inp23 = "", inp24 = "", inp25 = "0", inp26 = "", inp27 = "", inp29 = "";
        try {
            int i = 0;
            for (Object input : completeData) {
                if (i == 0) {
                    inp = input.toString();
                }
                if (i == 1) {
                    inp1 = input.toString();
                }
                if (i == 2) {
                    inp2 = input.toString();
                }
                if (i == 3) {
                    inp4 = input.toString();
                }
                
                if (i == 4) {
                    inp6 = input.toString();
                }
                if (i == 5) {
                    inp7 = input.toString();
                }
                if (i == 6) {
                    inp10 = input.toString();
                }
                if (i == 7) {
                    inp11 = input.toString();
                }
                if (i == 8) {
                    inp12 = input.toString();
                }
                if (i == 9) {
                    inp13 = input.toString();
                }
                if (i == 10) {
                    inp14 = input.toString();
                }
                if (i == 11) {
                    inp15 = input.toString();
                }
                if (i == 12) {
                    inp16 = input.toString();
                }
                if (i == 13) {
                    inp17 = input.toString();
                }
                if (i == 14) {
                    inp18 = input.toString();
                }
                if (i == 15) {
                    inp19 = input.toString();
                }
                if (i == 16) {
                    inp20 = input.toString();
                }
                if (i == 17) {
                    inp21 = input.toString();
                }
                if (i == 18) {
                    inp22 = input.toString();
                }
                if (i == 19) {
                    inp23 = input.toString();
                }
                if (i == 20) {
                    inp24 = input.toString();
                }
                if (i == 21) {
                    inp25 = input.toString();
                }
                if (i == 22) {
                    inp26 = input.toString();
                }
                if (i == 23) {
                    inp27 = input.toString();
                }
                if (i == 24) {
                    inp29 = input.toString();
                }
                i++;
            }
            
            String cadenaV22220 = "";
            if (indexMapRVI != null) {
                cadenaV22220 = indexMapRVI;
                inp26 = cadenaV22220.substring(40, 52);
                inp26 = String.valueOf(signedAmountEspecial(inp26, logger));
            }
            
            data = new Object[]{inp,
                inp1, inp2, inp4, inp6, inp7, inp10,
                inp11, inp12, inp13, inp14, inp15, inp16, inp17, inp18, inp19, inp20,
                inp21, inp22, inp23, inp24, inp25, inp26, inp27, inp29};
            return data;
        } catch (Exception e) {
            logger.log(e.getLocalizedMessage());
            return null;
        }
    }
    
    public static boolean createFileTemp(Map<String, Object[]> data, String nameFile, LambdaLogger logger, String filePath) {
        boolean fileCreatedOk = true;
        try {
            List<String[]> dataFormat = new ArrayList<String[]>();
            
            logger.log("Creating tc33 report file");
            int count = 1;
            for (Map.Entry<String, Object[]> entry : data.entrySet()) {
                Object[] objArr = data.get(entry.getKey());
                String[] csv = completeString(objArr, logger);
                dataFormat.add(csv);
                if (count > 3000) {
                    break;
                }
                
            }

            File file = new File(filePath + nameFile + ".csv");
            try (FileWriter outputfile = new FileWriter(file); 
                 CSVWriter writer = new CSVWriter(outputfile)) {
                
                writer.writeAll(dataFormat);
            }
            File source_dir = new File("/tmp");
            listFiles(source_dir, logger);
            logger.log("CSV file written successfully on disk.");
            return fileCreatedOk;
            
        } catch (IOException e) {
            //e.printStackTrace();
            logger.log(e.getLocalizedMessage());
            return !fileCreatedOk;
        } 
            
    }
    
    public static String maskCard(String value) {
        value = value.trim();
//        return value;
        if (value == null) {
            return "";
        }
        if (value.equals("")) {
            return "";
        }
        if (!isCardValidate(value)) {
            return "";
        }
        char replaceCharacter = '*';
        int cardLength = value.length();
        String bin = value.trim().substring(0, 6);
        String lastDigits = value.trim().substring(cardLength - 4);
        int characterRest = cardLength - bin.length() - lastDigits.length();
        String mask = "";
        for (int i = 0; i < characterRest; i++) {
            mask += replaceCharacter;
        }
        return String.format("%s%s%s", bin, mask, lastDigits);
    }
    
    private static boolean isCardValidate(String strNum) {
        try {
            Long number = Long.parseLong(strNum.trim());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
        
    }
    
    private static Double signedAmount(String valor) {
        String amountOrigin = valor;
        Double amount = 0.00;
        amountOrigin = amountOrigin.substring(0, 10) + "." + amountOrigin.substring(10);
        switch (valor.charAt(valor.length() - 1)) {
            case '{':
                amountOrigin = amountOrigin.replace('{', '0');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'A':
                amountOrigin = amountOrigin.replace('A', '1');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'B':
                amountOrigin = amountOrigin.replace('B', '2');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'C':
                amountOrigin = amountOrigin.replace('C', '3');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'D':
                amountOrigin = amountOrigin.replace('D', '4');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'E':
                amountOrigin = amountOrigin.replace('E', '5');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'F':
                amountOrigin = amountOrigin.replace('F', '6');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'G':
                amountOrigin = amountOrigin.replace('G', '7');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'H':
                amountOrigin = amountOrigin.replace('H', '8');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case 'I':
                amountOrigin = amountOrigin.replace('I', '9');
                amount = Double.parseDouble(amountOrigin);
                break;
            
            case '}':
                amountOrigin = amountOrigin.replace('}', '0');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'J':
                amountOrigin = amountOrigin.replace('J', '1');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'K':
                amountOrigin = amountOrigin.replace('K', '2');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'L':
                amountOrigin = amountOrigin.replace('L', '3');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'M':
                amountOrigin = amountOrigin.replace('M', '4');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'N':
                amountOrigin = amountOrigin.replace('N', '5');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'O':
                amountOrigin = amountOrigin.replace('O', '6');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'P':
                amountOrigin = amountOrigin.replace('P', '7');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'Q':
                amountOrigin = amountOrigin.replace('Q', '8');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
            
            case 'R':
                amountOrigin = amountOrigin.replace('R', '9');
                amount = Double.parseDouble(amountOrigin) * -1;
                break;
        }
        return amount;
    }
    
    private static Double signedAmountEspecial(String valor, LambdaLogger logger) {
        String newValue = valor.replace("D", "");
        newValue = newValue.replace("C", "");
        Double amount;
        try {
            amount = Double.parseDouble(newValue);
            amount = amount / 1000000;
            return amount;
        } catch (NumberFormatException e) {
            logger.log("OLD AMOUNT: " + valor);
            amount = 0.0;
            return amount;
        }
    }
    
    private static String[] completeString(Object[] completeData, LambdaLogger logger) {
        String[] data = null;
        String inp = "", inp1 = "", inp2 = "", inp4 = "", inp6 = "", inp7 = "", inp10 = "";
        String inp11 = "", inp12 = "", inp13 = "", inp14 = "", inp15 = "", inp16 = "", inp17 = "", inp18 = "", inp19 = "", inp20 = "";
        String inp21 = "", inp22 = "", inp23 = "", inp24 = "", inp25 = "0", inp26 = "", inp27 = "", inp29 = "";
        try {
            int i = 0;
            for (Object input : completeData) {
                if (i == 0) {
                    inp = input.toString();
                }
                if (i == 1) {
                    inp1 = input.toString();
                }
                if (i == 2) {
                    inp2 = input.toString();
                }
                
                if (i == 3) {
                    inp4 = input.toString();
                }
                
                if (i == 4) {
                    inp6 = input.toString();
                }
                if (i == 5) {
                    inp7 = input.toString();
                }
                if (i == 6) {
                    inp10 = input.toString();
                }
                if (i == 7) {
                    inp11 = input.toString();
                }
                if (i == 8) {
                    inp12 = input.toString();
                }
                if (i == 9) {
                    inp13 = input.toString();
                }
                if (i == 10) {
                    inp14 = input.toString();
                }
                if (i == 11) {
                    inp15 = input.toString();
                }
                if (i == 12) {
                    inp16 = input.toString();
                }
                if (i == 13) {
                    inp17 = input.toString();
                }
                if (i == 14) {
                    inp18 = input.toString();
                }
                if (i == 15) {
                    inp19 = input.toString();
                }
                if (i == 16) {
                    inp20 = input.toString();
                }
                if (i == 17) {
                    inp21 = input.toString();
                }
                if (i == 18) {
                    inp22 = input.toString();
                }
                if (i == 19) {
                    inp23 = input.toString();
                }
                if (i == 20) {
                    inp24 = input.toString();
                }
                if (i == 21) {
                    inp25 = input.toString();
                }
                if (i == 22) {
                    inp26 = input.toString();
                }
                if (i == 23) {
                    inp27 = input.toString();
                }
                if (i == 24) {
                    inp29 = input.toString();
                }
                i++;
            }
            
            data = new String[]{inp,
                inp1, inp2, inp4, inp6, inp7, inp10,
                inp11, inp12, inp13, inp14, inp15, inp16, inp17, inp18, inp19, inp20,
                inp21, inp22, inp23, inp24, inp25, inp26, inp27, inp29};
            return data;
        } catch (Exception e) {
            logger.log(e.getLocalizedMessage());
            return null;
        }
    }
    
    public static void listFiles(File source_dir, LambdaLogger logger){
        if (source_dir.isDirectory()) {
            logger.log("Files in /tmp/ directory:");
            File[] oFilesDocuments = source_dir.listFiles();
            for (int i = 0; i < oFilesDocuments.length; i++) {
                File source_dir_list = new File(oFilesDocuments[i].getPath());
                if (!source_dir_list.isDirectory()) {
                    logger.log(oFilesDocuments[i].getPath());                        
                }
           }
        }
    }
    
}
