/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.incomingprocesstc33.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Estephany
 */
public class DateFormatUtil {
    public static Date formatStringtoDate(String format, String date) throws ParseException {
        
        Date tmp = null;
        DateFormat dateFormat = new SimpleDateFormat(format);
        try {
            tmp = dateFormat.parse(date);
        } catch (ParseException e) {
            tmp = dateFormat.parse(getDateTimeAPI());
        }
        return tmp;
    }
    
    public static String getDateTimeAPI() {
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    public static String formatDateToString(String format, Date date) {
        
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
    
}
